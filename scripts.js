// create a request variable and assign a new XMLHttpRequest object to it.
var request = new XMLHttpRequest();

// Open a new connx, using the GET request on the URL endpoint
request.open('GET', 'https://ghibliapi.herokuapp.com/films', true);

request.onload = function () {
    // Begin accessing JSON data here
    var data = JSON.parse(this.response);

    // if the request returns valid 200 response, collect and show data
        // else handle errors
    if (request.status >= 200 && request.status < 400){
        
        // for each movie, we collect, organize and display data into
        // elements created
        data.forEach(movie => {
            // create the individual card element for data
            // to be viewed in
            const card = document.createElement('div');
            // set the attribute of the card div element to class w/ card
            card.setAttribute('class', 'card');
            // create the h1 element
            const h1 = document.createElement('h1');
            // assign the movie.title to the h1 element with textContent
            h1.textContent = movie.title;
            // create the p element with createElement
            const p = document.createElement('p');
            // assign the movies short description to the movie.description var
            movie.description = movie.description.substring(0,300);
            // set the textContent of p to the movie description
            // using string interpolation ${...} in JS
            p.textContent = `${movie.description}...`; // end w/ an elipses

            // appendChild(card) to the container
            container.appendChild(card);

            // appendChild(h1) to the card
            card.appendChild(h1);
            // appendChild(p) to the card
            card.appendChild(p);
        });

    } else {
        const errorMessage = document.createElement('marquee');
        errorMessage.textContent = `Gah, it's not working!`;
        app.appendChild(errorMessage);
    }
}

// Send request
request.send();

// get app level element or "ROOT ELEMENT"
const app = document.getElementById('root');
// create logo element
const logo = document.createElement('img');
// assign the logo a source on the web server
logo.src = 'logo.png';

// create the container <div> inside the root element
const container = document.createElement('div');
// set the attributes of the container to be class="container"
container.setAttribute('class', 'container');

// appendChild(logo) to the application
app.appendChild(logo);
// appendChild(container) to the application
app.appendChild(container);

// fin